import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom'
import './App.css';
import Header from './composants/Header.js';
import Home from './composants/Home.js';
import Browser from './composants/Browser.js';
import User from './composants/User.js'
import Profile from './composants/Profile.js'
import Combat from './composants/Combat.js'
import Opponent from './composants/Opponent.js'
import Error from './composants/Error.js'
import './styles/css/bootstrap.min.css';

class App extends Component {
    constructor() {
        super();
        this.state = {
            userID: -1,
            logged: false,
        }
    }

componentWillMount() {
        const userID = localStorage.getItem('userID')
        const logged = localStorage.getItem('logged')
        if (logged) {
            this.setState({
                logged: true,
                userID: parseInt(userID)
            })
        }
   }

    render() {
        return (
        <div class="MyContainer">
            <Router>
                <div>
                    <Header logged = {this.state.logged} />
                    <Route exact path="/" render = {() => (<Home logged = {this.state.logged} /> )} />
                    <Route exact path="/login" render = { () => (<User></User>)} />
                    <Route exact path="/register" render = { () => (<User></User>)} />
                    <Route exact path="/logout" render = { () => (<User/>)} />
                    <Route exact path="/profile" render = { () => (<Profile userID = {this.state.userID} />)} />
                    <Route exact path="/combat" render = { () => (<Combat/>)} />
                    <Route path="/Oprofile/:id" component={Opponent} />
                    <Route path="/Error/:type" component={Error} />
                    <Route path="/browser" component={Browser} />
                </div>
            </Router>
        </div>
    );
  }
}

export default App;
