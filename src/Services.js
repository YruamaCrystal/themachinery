import request from "./../node_modules/superagent/superagent";
import Config from'./Config.js';

function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

export function Connection(email, password) {

	if (validateEmail(email)) {
	fetch(Config.apiUrl + 'api/v1/connection/'+ email + '-' + password)
        .then(results => {
            return results.json();
        }).then(data => {
        	if (data === 1)
	        	ConnectionWork(email);
	        else
	        	window.location  = '/Error/Login';
       		})
        }
        else
        	window.location  = '/Error/Login';
}

export function ConnectionWork(username) {
	var userID;
	fetch(Config.apiUrl + 'api/v1/userID/'+ username)
        .then(results => {
            return results.json();
        }).then(data => {
            userID = data;
            localStorage.setItem('userID', userID)
			localStorage.setItem('logged', true)
			window.location  = '/Profile';
        })
}

export function Registration(username, password, btag) {
	request
	.post(Config.apiUrl + 'api/v1/registration')
	.set('Content-Type', 'application/x-www-form-urlencoded')
	.send({ username: username,
		password: password,
	    btag: btag })
	.end(function(err, res) {
		window.location  = '/';
	});  
}
