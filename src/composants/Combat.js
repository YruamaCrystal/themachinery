import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import '../App.css';
import Config from '../Config.js'

class Combat extends Component {
    constructor(props) {
        super(props);
        this.state = {
	    	stats: [],
	    	result: [],
	    	fight: false,
	    }
  }

componentWillMount() {
	var userID = localStorage.getItem('userID');
	   fetch(Config.apiUrl + 'api/v1/opponent/' + userID)
		.then(results => {
            return results.json();
        }).then(data => {
        	data = JSON.stringify(data);
        	data = JSON.parse(data);

        	var tmpArray = [];
        	let i = 0;

        	while (i < data.length)
        	{
        		tmpArray[i] = [data[i]['bnet_tag'], data[i]['score'], data[i]['id_users']];
        		i = i + 1;
        	}

        	let allState = tmpArray.map((stat) => 
        	{
        		return (
        			<tr>
        				<td>{stat[0]}</td>
        				<td>{stat[1]}</td>
        				<td>  <button
        					    className="btn btn-primary"
        					    value={stat[2]}
        					    onClick={(event) => this.Fight(event)}>
        					    Fight
        					</button></td>
        			</tr>
        			)
        	})	
        	this.setState({stats: allState});
        })
	}
	
	Fight(event) {
		this.setState({fight: true})
		var userID = localStorage.getItem('userID');
		var opponentID = event.target.getAttribute("value");

		 fetch(Config.apiUrl + 'api/v1/fight/' + userID +'-' + opponentID)
		.then(results => {
            return results.json();
        }).then(data => {
        	data = JSON.stringify(data);
        	data = JSON.parse(data);

        	var tmpArray = [];
        	let i = 0;

        	while (i < data.length)
        	{
        		tmpArray[i] = [data[i]['nb'], data[i]['result'], data[i]['pHeroes'], data[i]['oHeroes'], data[i]['pHeroesP'], data[i]['oHeroesP']];
        		i = i + 1;
        	}

        	let allState = tmpArray.map((stat) => 
        	{
        		return (
        			<tr className={stat[1]}>
        				<td>{stat[0]}</td>
        				<td>{stat[2]}</td>
        				<td>{stat[4]}</td>
        				<td>{stat[5]}</td>
        				<td>{stat[3]}</td>
        			</tr>
        			)
        	})	
        	this.setState({result: allState});
        })
	}

	fightArray() {
		if (this.state.fight) {
			return (
				<div>
        			Result
        	        <table class="table">
        	        	<thead>
        				    <tr>
        				      <th>#</th>
        				      <th>You</th>
        				       <th>Power</th>
        				        <th>Power</th>
        				      <th>Opponent</th>
        				    </tr>
        				  </thead>
        	        	<tbody>
        	        	 	{this.state.result} 
        	        	</tbody>
        	        </table>
        	    </div>
			)
		}
	}

	opponentArray() {
		if (!this.state.fight)
		return (
			<div>
			Opponents Available
	        <table class="table">
	        	<thead>
				    <tr>
				      <th>Name</th>
				      <th>Score</th>
				      <th>Fight</th>
				    </tr>
				  </thead>
	        	<tbody>
	        	 	{this.state.stats} 
	        	</tbody>
	        </table>
	        </div>
			)
	}

  render () {
    return (
      <div className="App">
        <h1>Combat Page</h1>
       	{this.opponentArray()}
       	{this.fightArray()}
      </div>
    )
  }
}

export default Combat;