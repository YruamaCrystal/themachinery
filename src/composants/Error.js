import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Connection, Registration } from '../Services.js'
import logo from '../logo.svg'

class Error extends Component {
	
	redirect() {
		if (this.props.match.params.type == "Login") {
			setTimeout(function(){
			    window.location = '/login';
			}, 1000);
		}
	}

	render () {
    	return (
      		<div className="container">
				<h1>Error {this.props.match.params.type}</h1>
				{this.redirect()}
      		</div>
    	)
 	}

}

export default Error;