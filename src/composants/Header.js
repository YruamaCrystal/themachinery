import React, { Component } from 'react'
import logo from '../logo.svg'
import '../App.css'
import '../styles/css/bootstrap.min.css'

import { BrowserRouter as Router, Route, Link } from 'react-router-dom'

class Header extends Component {

  getMenu() {
    if (this.props.logged === true) {
      return (
         <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item ">
                <Link class="nav-link" to={process.env.PUBLIC_URL + '/'}>Home</Link>
              </li>
              <li class="nav-item ">
                <Link class="nav-link" to={process.env.PUBLIC_URL + '/profile'}>Profile</Link>
              </li>
              <li class="nav-item">
                <Link class="nav-link" to={process.env.PUBLIC_URL + '/combat'}>Combat</Link>
              </li>
              <li class="nav-item ">
                <Link class="nav-link" to={process.env.PUBLIC_URL + '/browser'}>Pokewatcher</Link>
              </li>
              <li class="nav-item">
                <Link class="nav-link" to={process.env.PUBLIC_URL + '/logout'}>Logout</Link>
              </li>
            </ul>
          </div>
        </nav>
        )
    }
    else
      return (
        <nav class="navbar navbar-expand-lg navbar-light bg-light">
          <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
              <li class="nav-item ">
                <Link class="nav-link" to={process.env.PUBLIC_URL + '/'}>Home</Link>
              </li>
              <li class="nav-item ">
                <Link class="nav-link" to={process.env.PUBLIC_URL + '/browser'}>Pokewatcher</Link>
              </li>
              <li class="nav-item ">
                <Link class="nav-link" to={process.env.PUBLIC_URL + '/login'}>Login</Link>
              </li>
              <li class="nav-item">
                <Link class="nav-link" to={process.env.PUBLIC_URL + '/register'}>Register</Link>
              </li>

            </ul>
          </div>
        </nav>
        )
  }

   render () {
    return (
      <div className="App">
        <div className="App-header">
          <div className="App-brand">
            <Link to={process.env.PUBLIC_URL + '/'}>
              <img src={logo} className="App-logo" alt="logo" />
            </Link>
            <h2>PokeWatch</h2>
          </div>
        </div>
         <div>
            {this.getMenu()}
        </div>
      </div>
    )
  }
}

export default Header;