import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Config from '../Config.js'

class Home extends Component {
constructor(props) {
    super(props);
    this.state = {
	    	stats: [],
	    }
  }

componentWillMount() {
	   fetch(Config.apiUrl + 'api/v1/topusers/')
		.then(results => {
            return results.json();
        }).then(data => {
        	data = JSON.stringify(data);
        	data = JSON.parse(data);

        	var tmpArray = [];
        	let i = 0;

        	while (i < data.length)
        	{
        		tmpArray[i] = [ data[i]['bnet_tag'], data[i]['score'], data[i]['id_users']];
        		i = i + 1;
        	}

        	let allState = tmpArray.map((stat) => 
        	{
        		return (
        			<tr>
        				<td>
        					<Link class="nav-link" to={process.env.PUBLIC_URL + '/Oprofile/' + stat[2]}>{stat[0]}</Link>
        				</td>
        				<td>{stat[1]}</td>
        			</tr>
        			)
        	})
        	this.setState({stats: allState});
        })
	}

  render () {
    return (
      <div className="App">
        <h1>Home Page</h1>
       	Top 10 best Pokewatcher !
        <table class="table">
        	<thead>
			    <tr>
			      <th>Nom</th>
			      <th>Score</th>
			    </tr>
			  </thead>
        	<tbody>
        	 	{this.state.stats} 
        	</tbody>
        </table>
      </div>
    )
  }
}

export default Home;