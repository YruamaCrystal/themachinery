import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Config from '../Config.js'

class Profile extends Component {
	constructor(props) {
	    super(props);
	    this.state = {
	    	heros: [],
	    	datas: [],
	    	hero: false,
	    	data: true,
	    }
	}
	
	componentWillMount() {
		var userID = localStorage.getItem('userID');
	   	fetch(Config.apiUrl + 'api/v1/stats/hero/' + userID)
		.then(results => {
            return results.json();
        }).then(data => {
        	data = JSON.stringify(data);
        	data = JSON.parse(data);

        	var tmpArray = [];
        	let i = 0;

        	while (i < data.length)
        	{
        		tmpArray[i] = [ data[i]['name'], data[i]['power'], data[i]['portrait'], data[i]['shiny']];
        		i = i + 1;
        	}

        	let allState = tmpArray.map((stat) => 
        	{
        		return (
        			<tr>
        				<td>{stat[0]}</td>
        				<td>{stat[1]}</td>
        				<td><img src={stat[2]}/></td>
        				<td>{stat[3]}</td>
        			</tr>
        			)
        	})	
        	
        	this.setState({heros: allState});
        })

         fetch(Config.apiUrl + 'api/v1/stats/data/' + userID)
		.then(results => {
            return results.json();
        }).then(data => {
        	data = JSON.stringify(data);
        	data = JSON.parse(data);

        	let allState = [
        		data["username"],
        		data["heroName"],
        		data["heroPortrait"],
        		data["ratio"],
        		data["win"],
        		data["lose"],
        		data["equality"]
        	];

        	this.setState({datas: allState});
		})

	}
	
	displayHero() {
		if (this.state.hero) {
				return (
				<table class="table">
	        		<thead>
					    <tr>
					      <th>Nom</th>
					      <th>Puissance</th>
					      <th>Portrait</th>
					      <th>Shiny</th>
					    </tr>
					  </thead>
	        		<tbody>
	        		 	{this.state.heros} 
	        		</tbody>
	        	</table>
	        )
		}
	}

	displayData() {
		if (this.state.data) {
			return (
				<div className="container">
					<div class="row">
						<div className="col-6">
							<h1>{this.state.datas[0]}</h1>
							<p>Favorite hero: {this.state.datas[1]}</p>
							<img src={this.state.datas[2]} />
						</div>
						<div className="col-6">
							<div class="alert alert-info" role="alert">
							  <strong>Score : </strong> {this.state.datas[3]}
							</div>
							<div class="alert alert-success" role="alert">
							  <strong>Win : </strong> {this.state.datas[4]}
							</div>
							<div class="alert alert-warning" role="alert">
							  <strong>Equality : </strong> {this.state.datas[6]}
							</div>
							<div class="alert alert-danger" role="alert">
							  <strong>Lose : </strong> {this.state.datas[5]}
							</div>
						</div>
					</div>
				</div>
			)
		}
	}

	data() {
		this.setState({hero: false});
		this.setState({data: true});
	}

	hero() {
		this.setState({hero: true});
		this.setState({data: false});
	}

	header() {
		return (
			<div className="container">
				<div class="row">
					<div className="col-6">
						<button
		        		    className="btn btn-primary"
		        		    onClick={(event) => this.data(event)}>
		        		    Player Infos
		        		</button>
		        	</div>
		        	<div className="col-6">
		        		<button
		        		    className="btn btn-primary"
		        		    onClick={(event) => this.hero(event)}>
		        		    Player Heroes
		        		</button>
		        	</div>
	        	</div>
        	</div>
		)
	}

  render () {
    	return (
      		<div className="App">
	      		<br />
    			<h1>Profile</h1>
    			{this.header()}
    			<br />
       			{this.displayHero()}
       			{this.displayData()}   
      		</div>
    	)
  	}
}

export default Profile;