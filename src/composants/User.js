import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { Connection, Registration } from '../Services.js'
import logo from '../logo.svg'

class User extends Component {
	
	constructor(props) {
		super(props);
		this.state = {
            username: '',
            password: '',
            btag: '',
            fail: false,
            btagValue: false,
            load: false,
            userData: false,
            userGame: false
        	}
		}

	logout() {
		if (window.location.pathname === '/logout') {
			localStorage.clear()
			window.location = '/';
			{this.props.s.logged = false}
		}
	}

	// GetEmailInput
	handleChangeEmail(event) {
		this.setState({load: false})
    	this.setState({ username: event.target.value })
  	}

	handleChangePassword(event) {
    	this.setState({ password: event.target.value })
  	}

	handleChangebtag(event) {
    	this.setState({ btag: event.target.value })
  	}

  	// Transfert de la valeur temporaire à la valeur finale
  	submitForm(event) {
    	event.preventDefault()
    	this.setState({ name: this.state.tmpName})
  	}

	register() {
		if (window.location.pathname === '/register' && !this.state.load) {
			return (
					<div className="col-6 offset-md-3 Myform">
					<h1> Register </h1>
						<form>
  							<div class="form-group">
    							<label for="InputEmail">Email address</label>
    							<input onChange={(event) => this.handleChangeEmail(event)} type="text" class="form-control" id="InputEmail" aria-describedby="emailHelp" placeholder="Enter email" />
  							</div>
  							<div class="form-group">
    							<label for="Inputbtag">Bnet Tag</label>
    							<input onChange={(event) => this.handleChangebtag(event)} type="text" class="form-control" id="Inputbtag" aria-describedby="btagHelp" placeholder="Enter Bnet Tag" />
  							</div>
							<div class="form-group">
								<label for="InputPassword">Password</label>
								<input onChange={(event) => this.handleChangePassword(event)} type="password" class="form-control" id="exampleInputPassword" placeholder="Password" />
							</div>
							 <button
        					    className="btn btn-primary"
        					    onClick={(event) => this.SignUp(event)}>
        					    Sign up
        					</button>
						</form>
					</div>
				)
		}
	}

	login() {
		if (window.location.pathname === '/login' && !this.state.load) {
			return (
					<div className="col-6 offset-3">
					<h1> Login </h1>
						<form>
  							<div class="form-group">
    							<label for="InputEmail1">Email address</label>
    							<input onChange={(event) => this.handleChangeEmail(event)} type="text" class="form-control" id="InputEmail1" aria-describedby="emailHelp" placeholder="Enter email" />
  							</div>
							<div class="form-group">
								<label for="InputPassword1">Password</label>
								<input onChange={(event) => this.handleChangePassword(event)} type="password" class="form-control" id="exampleInputPassword1" placeholder="Password" />
							</div>
							 <button
        					    className="btn btn-primary"
        					    onClick={(event) => this.SignIn(event)}>
        					    Sign in
        					</button>
						</form>
					</div>
				)
		}
	}

	SignIn(event) {
		event.preventDefault()
		this.setState({fail: false});
		if (this.state.username === '' || this.state.password === '') {
			this.setState({fail: true});
		}
		else {
			this.setState({load: true})
			Connection(this.state.username, this.state.password)
		}
			

	}

	BtagVerificationUser(btag)
	{
	var arr = btag.split('#');
	return fetch('http://ow-api.herokuapp.com/profile/pc/eu/'+ arr[0] + "-" + arr[1])
        .then(results => {
            return results.json();
        }).then(data => {
        	if (data['username'])
				this.setState({userData: true})
			else
				this.setState({btagValue: true})
        })
        
	}

	BtagVerificationGame(btag)
	{
		var arr = btag.split('#');
		return fetch('http://ow-api.herokuapp.com/stats/pc/eu/'+ arr[0] + "-" + arr[1])
        .then(results => {
            return results.json();
        }).then(data => {
        	if (data['username'])
				this.setState({userGame: true})
			else
				this.setState({btagValue: true})
        })
        
	}

	SignUp(event) {
		event.preventDefault(this.state.btag)
		this.setState({fail: false});
		this.setState({btagValue: false});
		this.setState({userData: false})
		this.setState({userGame: false})
		if (this.state.username === '' || this.state.btag === '' ||this.state.password === '') {
			this.setState({fail: true});
		}
		else {
			this.setState({load: true})
			this.BtagVerificationUser(this.state.btag);
			this.BtagVerificationGame(this.state.btag);
		}

		if (this.state.userData && this.state.userGame) {
			Registration(this.state.username, this.state.password, this.state.btag);
		}
		else {
			this.setState({btagValue: true});
			this.setState({load: false})
		}
			
	}

	// Renvoie un message d'erreur
	getWarning() {
	  	if (this.state.fail)
	    	return (
	      		<p> You have to complete all.</p>
	    	)
	    if (this.state.btagValue)
	    	return (
	      		<p> Ce compte battlnet existe pas.</p>
	    	)
	  }

	load() {
		if (this.state.load)
		return (
			<div className="container">	
				<h2>Loading ...</h2>
				<img src={logo} className="App-logo" alt="logo" />
			</div>
		)
	}

	getData() {
	return (
		<div>
    		{this.logout()}
    		{this.login()}
    		{this.register()}
    		{this.getWarning()}
    		{this.load()}
    	</div>
		)
	}

	render () {
    	return (
      		<div>
      			{this.getData()}
      		</div>
    	)
 	}
}

export default User;